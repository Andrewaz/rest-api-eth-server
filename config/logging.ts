const winston = require('winston');

// Logger configuration
const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: './logs/log.log'
        })
    ]
};
const date = new Date();
const hour = date.getHours();
const minutes = formatMinutes(date.getMinutes().toString());
const seconds = formatSeconds(date.getHours().toString());
// Create the logger
export const logger = winston.createLogger(logConfiguration);

export function logApp(message: string) {
    logger.info(date.toDateString() +
        hour + ":" +
        minutes + ":" +
        seconds + '  ' +
        message);
}

function formatMinutes(minutes: string) {
    if (parseFloat(minutes) < 10) {
        return '0' + minutes;
    }
    return minutes;
}

function formatSeconds(seconds: string) {
    if (parseFloat(seconds) < 10) {
        return '0' + seconds;
    }
    return seconds;
}
