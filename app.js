"use strict";
exports.__esModule = true;
var express = require("express");
var transactions_1 = require("./api/routes/transactions/transactions");
exports.app = express();
// handling the transaction request
exports.app.use('/transactions', transactions_1.router);
