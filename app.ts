import express = require("express");
import {router} from "./api/routes/transactions/transactions";

export const app = express();
app.use('/transactions', router);

