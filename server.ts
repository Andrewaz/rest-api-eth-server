import * as http from "http";
import {app} from "./app";
import {logApp} from "./config/logging";

const port = process.env.PORT || 3003;
const server = http.createServer(app);
server.listen(port);


logApp('starting Server');

