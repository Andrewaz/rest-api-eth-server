"use strict";
exports.__esModule = true;
var express = require("express");
exports.router = express.Router();
exports.router.post('/', function (req, res, next) {
    res.status(200).json({
        message: 'handling POST request to transactions'
    });
});
exports.router.get('/:walletAddress', function (req, res, next) {
    res.status(200).json({
        message: 'handling GET request to transaction',
        walletAddress: req.params.walletAddress
    });
});
