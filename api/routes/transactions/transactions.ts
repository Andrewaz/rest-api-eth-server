import * as express from "express";

export const router = express.Router();

router.post('/', (req, res, next) => {
    res.status(200).json({
        message: 'handling POST request to transactions'
    });
});

router.get('/:walletAddress', (req, res, next) => {
    res.status(200).json({
        message: 'handling GET request to transaction',
        walletAddress: req.params.walletAddress
    });
});
